const express = require("express");
const router = express.Router();

let { UserGame } = require("../models");

router.get("/user_game", async (req, res) => {
  const result = await UserGame.findAll({ raw: true });
  console.log(result)
  res.render("adminPanel", { data: result });
});

router.get('/user_game/deleteAll', async (req,res) => {
    UserGame.destroy({
        where: {},
        truncate: true
      });
      res.redirect('/user_game')
})

router.post("/user_game", async (req, res) => {
  const { id, username, password } = req.body;
  const { method } = req.query;
  if (!method) {
    await UserGame.create({
        id: id,
      username: username,
      password: password,
    });
  } else if (method == "PUT") {
    await UserGame.update(
      { username, password },
      { where: { id: parseInt(id) } }
    );
  }

  res.redirect("/user_game");
});

router.post("/user_game/delete", async function (req, res) {
  const { id } = req.body;

  const result = await UserGame.destroy({
    where: { id: id },
  });

  res.redirect("/user_game");
});

router.post("/user_game/edit", async function (req, res) {
  const { id } = req.body;

  const dataToEdit = await UserGame.findOne({
    where: { id: id },
    raw: true,
  });
  console.log("User edit:", dataToEdit);

  res.render("edit", { dataToEdit });
});

module.exports = router;
