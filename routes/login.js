const express = require("express");
const router = express.Router();
const fs = require("fs");

router.use(express.urlencoded({ extended: true }));
router.use(express.json());

router.get("/login", (req, res) => {
  res.status(200).render('login');
});

router.post("/login", (req, res) => {
  const { username, password } = req.body;

  let admins = require("../admin.json");
  console.log(admins);
  const adminLogin = admins.find(
    (item) => item.username == username && item.password == password
  );
  console.log(adminLogin);
  if (!adminLogin) {
    console.log("not an admin, redirect to login");
    res.redirect("/login");
  } else {
    console.log(
      "Hello " + adminLogin + " Welcome to admin page"
    );
    res.redirect("/user_game");
  }
});

module.exports = router;
