const express = require('express');
const app = express();
const port = 3000;
const fs = require('fs');


app.set('view engine','ejs');
app.use(express.urlencoded({extended: true}))
app.use(express.json());
app.use(express.static('public'))

const loginRoutes = require('./routes/login');
const panelRoutes = require('./routes/adminPanel');

app.use(loginRoutes);
app.use(panelRoutes);

app.get('/', (req,res) => {
    res.status(200).redirect('/login');
})

app.get("*", (req,res)=>{
    res.status(404).send("page doesnt exist!");
})

app.listen(port, () => console.log(`app listenting at ${port}`))